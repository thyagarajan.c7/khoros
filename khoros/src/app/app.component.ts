import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  index: any = 0;
  defaultCount = 4;
  carElems = [{ name: 'abcd1' }, { name: 'abcd2' }, { name: 'abcd3' }, { name: 'abcd4' }, { name: 'abcd5' }, { name: 'abcd6' }];
  elms: any = [];

  ngOnInit(): void {
    this.getIndexItems();
  }

  getIndexItems() {
    this.elms = [];
    for (let el in this.carElems) {
      console.log(el, this.index);
      if (el >= this.index && el < this.index + this.defaultCount) {
        this.elms.push(this.carElems[el]);
      }
    }
    console.log(this.elms, 'carelemts');
  }

  prev() {
    console.log(this.index);
    if (this.index > 0) {
      this.index = this.index - 1;
      this.getIndexItems();
    }
  }
  next() {
    if (this.index + this.defaultCount < this.carElems.length) {
      this.index = this.index + 1;
      this.getIndexItems();
    }
  }
}
